

Updating the configuration with Terraform
=========================================


Prerequisites
-------------

Terraform installed.

Run Powershell and change directory to this one

```shell
terraform init      # to download the provider
terraform fmt       # to format source files
terraform validate  # to check that the structure is OK
terraform apply     # to run
   # Type "yes" when prompted. 
   # Check your created infrastructure at the DNS Zones
```


Optional:

```shell
terraform destroy # to remove all the created infrastructure
```