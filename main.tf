terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.98.0"
    }
  }
}

provider "azurerm" {
  features {}

  storage_use_azuread = true
  subscription_id     = local.subscription_id
}

resource "azurerm_dns_zone" "domain" {
  name                = local.domain_name
  resource_group_name = local.resource_group_name
}

# A record(s)
resource "azurerm_dns_a_record" "a" {
  name                = "@"
  zone_name           = azurerm_dns_zone.domain.name
  resource_group_name = azurerm_dns_zone.domain.resource_group_name
  ttl                 = local.ttl
  records             = ["127.0.0.1"]
}

# CAA records
resource "azurerm_dns_caa_record" "caa" {
  name                = "@"
  zone_name           = azurerm_dns_zone.domain.name
  resource_group_name = azurerm_dns_zone.domain.resource_group_name
  ttl                 = local.ttl

  record {
    flags = 0
    tag   = "issue"
    value = "digicert.com"
  }
}